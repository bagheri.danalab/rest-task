import { Test, TestingModule } from '@nestjs/testing';
import { UserRepository } from '../../repositories/user.repository';
import { UserService } from '../../modules/user/user.service';
import { ConfigService } from '../../config/config.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ConfigModule } from '../../config/config.module';
import { MailModule } from '../../modules/mail/mail.module';
import { Avatar, AvatarSchema } from '../../entities/avatar.entity';
import { User, UserSchema } from '../../entities/user.entity';
import { MongooseModule } from '@nestjs/mongoose';

describe('UserService', () => {
    let service: UserService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRootAsync({
                    inject: [ConfigService],
                    useFactory: async (configService: ConfigService) => configService.getMongoConfig(),
                }),
                MongooseModule.forFeature([
                    { name: User.name, schema: UserSchema },
                    { name: Avatar.name, schema: AvatarSchema },
                ]),
                MailModule,
                ClientsModule.registerAsync([
                    {
                        name: 'RABBITMQ',
                        imports: [ConfigModule],
                        useFactory: (configService: ConfigService) => ({
                            transport: Transport.RMQ,
                            options: {
                                urls: [`${configService.get('RABBITMQ_HOST')}`],
                                queue: `${configService.get('RABBITMQ_QUEUE_NAME')}`,
                                queueOptions: {
                                    durable: false,
                                },
                            },
                        }),
                        inject: [ConfigService],
                    },
                ]),
            ],
            providers: [UserService, UserRepository],
        }).compile();

        service = module.get<UserService>(UserService);
    });
    jest.setTimeout(20000); // for poor connections

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
    it('should create a user', async () => {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < 5) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }

        const payload = {
            first_name: 'string',
            email: `${result}@string.com`,
            last_name: 'string',
            avatar: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAApgAAAKYB3X3/OAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAANCSURBVEiJtZZPbBtFFMZ/M7ubXdtdb1xSFyeilBapySVU8h8OoFaooFSqiihIVIpQBKci6KEg9Q6H9kovIHoCIVQJJCKE1ENFjnAgcaSGC6rEnxBwA04Tx43t2FnvDAfjkNibxgHxnWb2e/u992bee7tCa00YFsffekFY+nUzFtjW0LrvjRXrCDIAaPLlW0nHL0SsZtVoaF98mLrx3pdhOqLtYPHChahZcYYO7KvPFxvRl5XPp1sN3adWiD1ZAqD6XYK1b/dvE5IWryTt2udLFedwc1+9kLp+vbbpoDh+6TklxBeAi9TL0taeWpdmZzQDry0AcO+jQ12RyohqqoYoo8RDwJrU+qXkjWtfi8Xxt58BdQuwQs9qC/afLwCw8tnQbqYAPsgxE1S6F3EAIXux2oQFKm0ihMsOF71dHYx+f3NND68ghCu1YIoePPQN1pGRABkJ6Bus96CutRZMydTl+TvuiRW1m3n0eDl0vRPcEysqdXn+jsQPsrHMquGeXEaY4Yk4wxWcY5V/9scqOMOVUFthatyTy8QyqwZ+kDURKoMWxNKr2EeqVKcTNOajqKoBgOE28U4tdQl5p5bwCw7BWquaZSzAPlwjlithJtp3pTImSqQRrb2Z8PHGigD4RZuNX6JYj6wj7O4TFLbCO/Mn/m8R+h6rYSUb3ekokRY6f/YukArN979jcW+V/S8g0eT/N3VN3kTqWbQ428m9/8k0P/1aIhF36PccEl6EhOcAUCrXKZXXWS3XKd2vc/TRBG9O5ELC17MmWubD2nKhUKZa26Ba2+D3P+4/MNCFwg59oWVeYhkzgN/JDR8deKBoD7Y+ljEjGZ0sosXVTvbc6RHirr2reNy1OXd6pJsQ+gqjk8VWFYmHrwBzW/n+uMPFiRwHB2I7ih8ciHFxIkd/3Omk5tCDV1t+2nNu5sxxpDFNx+huNhVT3/zMDz8usXC3ddaHBj1GHj/As08fwTS7Kt1HBTmyN29vdwAw+/wbwLVOJ3uAD1wi/dUH7Qei66PfyuRj4Ik9is+hglfbkbfR3cnZm7chlUWLdwmprtCohX4HUtlOcQjLYCu+fzGJH2QRKvP3UNz8bWk1qMxjGTOMThZ3kvgLI5AzFfo379UAAAAASUVORK5CYII=',
        };

        expect(await service.createUser(payload)).toEqual({
            first_name: expect.any(String),
            email: expect.any(String),
            last_name: expect.any(String),
            _id: expect.any(Object),
            createdAt: expect.any(Date),
            __v: expect.any(Number),
            avatar: expect.any(String),
        });
    });
    it('should get a user', async () => {
        expect(await service.getUserById('63fc722757be02cde8ef5627')).toEqual({
            first_name: expect.any(String),
            email: expect.any(String),
            last_name: expect.any(String),
            _id: expect.any(Object),
            createdAt: expect.any(Date),
            __v: expect.any(Number),
            avatar: expect.any(String),
        });
    });

    it('should get a avatar', async () => {
        const _id = '63fc722757be02cde8ef5627';

        expect(await service.getAvatarById(_id)).toEqual({
            avatar: expect.any(String),
        });
    });

    it('should delete a avatar', async () => {
        const _id = '63fc722757be02cde8ef5627';

        expect(await service.deleteAvatarById(_id)).toEqual({
            message: expect.any(String),
        });
    });
});
