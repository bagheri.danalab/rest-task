import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from '../../modules/user/user.service';
import { UserController } from '../../modules/user/user.controller';
import { Schema } from 'mongoose';
describe('UserController', () => {
    let Controller: UserController;

    const mockUsersService = {
        createUser: jest.fn((dto) => {
            return {
                _id: Date.now().toString(),
                createdAt: Date.now().toString(),
                __v: 0,
                ...dto,
            };
        }),
        getUserById: jest.fn(() => {
            return {
                _id: Date.now().toString(),
                createdAt: Date.now().toString(),
                __v: 0,
                first_name: 'string',
                email: 'string3@string.com',
                last_name: 'string',
                avatar: 'string',
            };
        }),
        getAvatarById: jest.fn(() => {
            return {
                avatar: 'string',
            };
        }),
        deleteAvatarById: jest.fn(() => {
            return {
                message: 'string',
            };
        }),
    };
    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [UserController],
            providers: [UserService],
        })
            .overrideProvider(UserService)
            .useValue(mockUsersService)
            .compile();

        Controller = module.get<UserController>(UserController);
    });
    jest.setTimeout(20000) // for poor connections

    it('should be defined', () => {
        expect(Controller).toBeDefined();
    });
    it('should create a user', async () => {
        const payload = {
            first_name: 'string',
            email: 'string3@string.com',
            last_name: 'string',
            avatar: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAApgAAAKYB3X3/OAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAANCSURBVEiJtZZPbBtFFMZ/M7ubXdtdb1xSFyeilBapySVU8h8OoFaooFSqiihIVIpQBKci6KEg9Q6H9kovIHoCIVQJJCKE1ENFjnAgcaSGC6rEnxBwA04Tx43t2FnvDAfjkNibxgHxnWb2e/u992bee7tCa00YFsffekFY+nUzFtjW0LrvjRXrCDIAaPLlW0nHL0SsZtVoaF98mLrx3pdhOqLtYPHChahZcYYO7KvPFxvRl5XPp1sN3adWiD1ZAqD6XYK1b/dvE5IWryTt2udLFedwc1+9kLp+vbbpoDh+6TklxBeAi9TL0taeWpdmZzQDry0AcO+jQ12RyohqqoYoo8RDwJrU+qXkjWtfi8Xxt58BdQuwQs9qC/afLwCw8tnQbqYAPsgxE1S6F3EAIXux2oQFKm0ihMsOF71dHYx+f3NND68ghCu1YIoePPQN1pGRABkJ6Bus96CutRZMydTl+TvuiRW1m3n0eDl0vRPcEysqdXn+jsQPsrHMquGeXEaY4Yk4wxWcY5V/9scqOMOVUFthatyTy8QyqwZ+kDURKoMWxNKr2EeqVKcTNOajqKoBgOE28U4tdQl5p5bwCw7BWquaZSzAPlwjlithJtp3pTImSqQRrb2Z8PHGigD4RZuNX6JYj6wj7O4TFLbCO/Mn/m8R+h6rYSUb3ekokRY6f/YukArN979jcW+V/S8g0eT/N3VN3kTqWbQ428m9/8k0P/1aIhF36PccEl6EhOcAUCrXKZXXWS3XKd2vc/TRBG9O5ELC17MmWubD2nKhUKZa26Ba2+D3P+4/MNCFwg59oWVeYhkzgN/JDR8deKBoD7Y+ljEjGZ0sosXVTvbc6RHirr2reNy1OXd6pJsQ+gqjk8VWFYmHrwBzW/n+uMPFiRwHB2I7ih8ciHFxIkd/3Omk5tCDV1t+2nNu5sxxpDFNx+huNhVT3/zMDz8usXC3ddaHBj1GHj/As08fwTS7Kt1HBTmyN29vdwAw+/wbwLVOJ3uAD1wi/dUH7Qei66PfyuRj4Ik9is+hglfbkbfR3cnZm7chlUWLdwmprtCohX4HUtlOcQjLYCu+fzGJH2QRKvP3UNz8bWk1qMxjGTOMThZ3kvgLI5AzFfo379UAAAAASUVORK5CYII=',
        };

        expect(await Controller.createUser(payload)).toEqual({
            first_name: expect.any(String),
            email: expect.any(String),
            last_name: expect.any(String),
            _id: expect.any(String),
            createdAt: expect.any(String),
            __v: 0,
            avatar: expect.any(String),
        });
        expect(mockUsersService.createUser).toHaveBeenCalled();
    });
    it('should get a user', async () => {
        const objectId = Schema.Types.ObjectId;
        const _id = new objectId('63fc611f3a257237e9931457');

        expect(await Controller.getUserById(_id)).toEqual({
            first_name: expect.any(String),
            email: expect.any(String),
            last_name: expect.any(String),
            _id: expect.any(String),
            createdAt: expect.any(String),
            __v: 0,
            avatar: expect.any(String),
        });
        expect(mockUsersService.getUserById).toHaveBeenCalled();
    });

    it('should get a avatar', async () => {
        const objectId = Schema.Types.ObjectId;
        const _id = new objectId('63fc611f3a257237e9931457');

        expect(await Controller.getAvatarById(_id)).toEqual({
            avatar: expect.any(String),
        });
        expect(mockUsersService.getAvatarById).toHaveBeenCalled();
    });

    it('should delete a avatar', async () => {
        const objectId = Schema.Types.ObjectId;
        const _id = new objectId('63fc611f3a257237e9931457');

        expect(await Controller.deleteAvatarById(_id)).toEqual({
            message: expect.any(String),
        });
        expect(mockUsersService.deleteAvatarById).toHaveBeenCalled();
    });
});
