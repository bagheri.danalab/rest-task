import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { IMailService } from '../../types/mail.services.interface';
import { User } from '../../types/mail.entity';

@Injectable()
export class MailService implements IMailService {
    constructor(private mailerService: MailerService) { }

    async sendUserConfirmation(user: User) {
        try {
            const url = `https://nestjs.com/`;

            await this.mailerService
                .sendMail({
                    to: user.email,
                    from: '"Support Team" <support@example.com>',
                    subject: 'Welcome to Nice App! Confirm your Email',
                    template: './temp',
                    context: {
                        name: user.first_name,
                        url,
                    },
                })
        } catch (error) {
            console.log(error);
        }
    }
}
