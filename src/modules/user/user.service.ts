import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices/client';
import { ClientSession, Schema as MongooseSchema } from 'mongoose';
import { IUserService } from '../../types/user.services.interface';
import { UserRepository } from '../../repositories/user.repository';
import { MailService } from '../mail/mail.service';
import { CreateUserDto } from './dto/createUser.dto';

@Injectable()
export class UserService implements IUserService {
    constructor(private readonly userRepository: UserRepository, private mailService: MailService, @Inject('RABBITMQ') private client: ClientProxy) {
        this.client.connect()
    }

    async createUser(createUserDto: CreateUserDto) {
        const createdUser = await this.userRepository.createUser(createUserDto);
        this.mailService.sendUserConfirmation({ first_name: createUserDto.first_name, email: createUserDto.email })
        this.client.emit('user-created', { first_name: createUserDto.first_name, email: createUserDto.email });
        return createdUser;
    }

    async getUserById(id: MongooseSchema.Types.ObjectId | string) {
        return await this.userRepository.getUserById(id);
    }

    async getAvatarById(id: MongooseSchema.Types.ObjectId | string) {
        return await this.userRepository.getAvatarById(id);
    }

    async deleteAvatarById(id: MongooseSchema.Types.ObjectId | string) {
        return await this.userRepository.deleteAvatarById(id);
    }
}
