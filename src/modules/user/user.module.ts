import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '../../config/config.module';
import { ConfigService } from '../../config/config.service';
import { Avatar, AvatarSchema } from '../../entities/avatar.entity';

import { User, UserSchema } from '../../entities/user.entity';
import { UserRepository } from '../../repositories/user.repository';
import { MailModule } from '../mail/mail.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: User.name, schema: UserSchema },
            { name: Avatar.name, schema: AvatarSchema },
        ]),
        MailModule,
        ClientsModule.registerAsync([
            {
              name: 'RABBITMQ',
              imports: [ConfigModule],
              useFactory: (configService: ConfigService) => ({
                transport: Transport.RMQ,
                options: {
                  urls: [`${configService.get('RABBITMQ_HOST')}`],
                  queue: `${configService.get('RABBITMQ_QUEUE_NAME')}`,
                  queueOptions: {
                    durable: false,
                  },
                },
              }),
              inject: [ConfigService],
            },
       ]),
    ],
    controllers: [UserController],
    providers: [UserService, UserRepository],
    exports: [UserService, UserRepository],
})
export class UserModule {}
