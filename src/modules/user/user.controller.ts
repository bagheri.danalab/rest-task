import { BadRequestException, Body, Controller, Delete, Get, HttpStatus, Param, Post, Res } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Response } from 'express';
import { Exception } from 'handlebars';
import { Connection, Schema as MongooseSchema } from 'mongoose';
import { createUserResponse } from 'src/types/createUserResponse';
import { CreateUserDto } from './dto/createUser.dto';
import { UserService } from './user.service';

@Controller('/api/user/')
export class UserController {
    constructor(
        // @InjectConnection() private readonly mongoConnection: Connection,  this is for Transaction in future if we want to use it
        private userService: UserService,
    ) {}

    @Post('/')
    async createUser(@Body() createUserDto: CreateUserDto, @Res() res?: Response) {
        // const session = await this.mongoConnection.startSession();
        // session.startTransaction();
        try {
            const newUser: Promise<createUserResponse | Exception> = await this.userService.createUser(
                createUserDto,
                // ,session
            );
            // await session.commitTransaction();
            if (res) {
                return res.status(HttpStatus.CREATED).send(newUser);
            } else {
                return newUser;
            }
        } catch (error) {
            // await session.abortTransaction();
            throw new BadRequestException(error);
        } finally {
            // session.endSession();
        }
    }

    @Get('/:id')
    async getUserById(@Param('id') id: MongooseSchema.Types.ObjectId, @Res() res?: Response) {
        const user: Promise<createUserResponse | Exception> = await this.userService.getUserById(id);
        if (res) {
            return res.status(HttpStatus.CREATED).send(user);
        } else {
            return user;
        }
    }
    @Get('/:id/avatar')
    async getAvatarById(@Param('id') id: MongooseSchema.Types.ObjectId, @Res() res?: Response) {
        const avatar: any = await this.userService.getAvatarById(id);
        if (res) {
            return res.status(HttpStatus.OK).send(avatar);
        } else {
            return avatar;
        }
    }

    @Delete('/:id/avatar')
    async deleteAvatarById(@Param('id') id: MongooseSchema.Types.ObjectId, @Res() res?: Response) {
        const message: any = await this.userService.deleteAvatarById(id);
        if (res) {
            return res.status(HttpStatus.OK).send(message);
        } else {
            return message;
        }
    }
}
