import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';

@Schema()
export class Avatar extends Document {
    @Prop({ required: true })
    url: string;

    @Prop({ type: SchemaTypes.ObjectId })
    user_id: Types.ObjectId;
}

export const AvatarSchema = SchemaFactory.createForClass(Avatar);
