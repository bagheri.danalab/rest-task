import { CreateUserDto } from '../modules/user/dto/createUser.dto';
import { ClientSession, Schema as MongooseSchema } from 'mongoose';
import { createUserResponse } from './createUserResponse';
import { Exception } from 'handlebars';
import { avatarUserResponse } from './avatarResponse';
import { deleteAvatarResponse } from './deleteResponse';

export interface IUserService {
    createUser(createUserDto: CreateUserDto): Promise<createUserResponse | Exception>;
    getUserById(id: MongooseSchema.Types.ObjectId | string): Promise<createUserResponse | Exception>;
    getAvatarById(id: MongooseSchema.Types.ObjectId | string): Promise<avatarUserResponse | Exception>;
    deleteAvatarById(id: MongooseSchema.Types.ObjectId | string): Promise<deleteAvatarResponse | Exception>;
}
