export interface createUserResponse {
    first_name: string;
    email: string;
    last_name: string;
    _id: string;
    createdAt: string;
    __v: number;
    avatar: string;
}
