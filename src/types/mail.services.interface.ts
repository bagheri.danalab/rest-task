import { User } from '../entities/user.entity';

export interface IMailService {
    sendUserConfirmation(user: User): void;
}
