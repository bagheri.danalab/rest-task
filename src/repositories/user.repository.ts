import { HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ClientSession, Model, Schema as MongooseSchema } from 'mongoose';
import { User } from '../entities/user.entity';
import { CreateUserDto } from '../modules/user/dto/createUser.dto';
import { writeFile, readFileSync, existsSync, unlinkSync, mkdirSync } from 'fs';
import { Avatar } from '../entities/avatar.entity';

export class UserRepository {
    constructor(@InjectModel(User.name) private readonly userModel: Model<User>, @InjectModel(Avatar.name) private readonly avatarModel: Model<Avatar>) {}

    async createUser(createUserDto: CreateUserDto) {
        let user = await this.getUserByEmail(createUserDto.email);

        if (user) {
            throw new HttpException('User already exists', HttpStatus.CONFLICT);
        }

        user = new this.userModel({
            first_name: createUserDto.first_name,
            last_name: createUserDto.last_name,
            email: createUserDto.email,
        });
        if (!createUserDto.avatar.includes('data:image/png;base64,')) {
            throw new HttpException('Format is not correct it must be base64/png', HttpStatus.BAD_REQUEST);
        }
        const base64Data = createUserDto.avatar.replace(/^data:image\/png;base64,/, '');
        if (!existsSync('S3')) {
            mkdirSync('S3');
        }
        writeFile(`S3/${user._id}.png`, base64Data, 'base64', function (err) {
            if (err) {
                throw new HttpException(err.message, HttpStatus.CONFLICT);
            }
        });
        let avatar = new this.avatarModel({
            user_id: user._id,
            url: `S3/${user._id}.png`,
        });
        try {
            user = await user.save({});
            avatar = await avatar.save({});
        } catch (error) {
            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (!user) {
            throw new HttpException('User not created', HttpStatus.NOT_FOUND);
        } else if (!avatar) {
            throw new HttpException('Avatar not created', HttpStatus.CONFLICT);
        }

        return { ...user._doc, avatar: createUserDto.avatar };
    }

    async getUserById(id: MongooseSchema.Types.ObjectId | string) {
        let user: any;
        let avatar: string;
        let avatarUrl: string;
        try {
            user = await this.userModel.findById({ _id: id });
            avatarUrl = (await this.avatarModel.findOne({ user_id: user._id }))?.url;

            try {
                avatar = 'data:image/png;base64,' + readFileSync(avatarUrl, 'base64');
            } catch (error) {
                console.log(error.message);
            }
            if (!user) {
                throw new HttpException('User not found', HttpStatus.NOT_FOUND);
            }

            return { ...user._doc, avatar: avatar ? avatar : '' };
        } catch (error) {
            console.log(error);

            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async getAvatarById(id: MongooseSchema.Types.ObjectId | string) {
        let avatar: any;
        let avatarUrl: any;
        try {
            avatarUrl = (await this.avatarModel.findOne({ user_id: id }))?.url;
            if (!avatar) {
                return { avatar: '' };
            }
            try {
                avatar = 'data:image/png;base64,' + readFileSync(avatarUrl, 'base64');
            } catch (error) {
                console.log(error.message);
            }
            return { avatar: avatar ? avatar : '' };
        } catch (error) {
            console.log(error);

            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async deleteAvatarById(id: MongooseSchema.Types.ObjectId | string) {
        let avatarUrl: any;
        try {
            avatarUrl = await this.avatarModel.findOneAndDelete({ user_id: id });
            if (!avatarUrl) {
                throw new HttpException('Avatar not found', HttpStatus.NOT_FOUND);
            }
            unlinkSync(avatarUrl?.url);

            return { message: 'Successfully deleted' };
        } catch (error) {
            return { message: 'There is noting to delete' };
        }
    }

    private async getUserByEmail(email: string) {
        let user: any;
        try {
            user = await this.userModel.findOne({ email }, 'name email img role').exec();
        } catch (error) {
            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return user;
    }
}
